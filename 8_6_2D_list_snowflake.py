"""Dado un número entero positivo impar n, produce una matriz
bidimensional de tamaño n × n. Rellene cada elemento con el
carácter '.' . Luego llene la fila central, la columna central
y las diagonales con el carácter '*'. Obtendrá una imagen de un
copo de nieve. Imprima el copo de nieve en n × n filas y columnas
y separe los caracteres con un solo espacio."""
num = int(input())
a = [["."]*num for i in range(num)]
for y in range(num):
    for x in range(num):
        if x == y:
            a[y][x] = '*'
        elif x == (num-1)/2:
            a[y][x] = '*'
        elif x + y == 6:
            a[y][x] = '*'
        elif y == (num-1)/2:
            a[y][x] = '*'
for var in a:
    print(*var)
