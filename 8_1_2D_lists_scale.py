# Ingresar el numero de filas y columnas
# Ingresar los numeros que iran dentro de la matriz
# Ingresar un numero que multipleque a la matriz
# Imprimir el resultado

n, m = [int(i) for i in input("Ingrsar el numero de filas columnas:\n").split()]
a = [[int(j) for j in input().split()] for i in range(n)]
times = int(input("Multiplicar por:"))

for x in range(n):
    for y in range(m):
        a[x][y] = ((a[x][y]) * times)

for row in a:
    print(' '.join([str(a) for a in row]))
