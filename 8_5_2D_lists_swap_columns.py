"""Given two integers - the number of rows m and columns n of m×n 2d
list - and subsequent m rows of n integers, followed by two non-negative
integers i and j less than n, swap the columns i and j of 2d list and
print the result."""

m, n = [int(s) for s in input().split()]
matriz = [[int(k) for k in input().split()] for i in range(m)]
a, b = [int(s) for s in input().split()]
for t in range(m):
    matriz[t][b], matriz[t][a] = matriz[t][a], matriz[t][b]
    print(*matriz[t], sep=' ')
